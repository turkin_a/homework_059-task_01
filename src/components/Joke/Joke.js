import React, { Component } from 'react';
import './Joke.css';

class Joke extends Component {
  render() {
    return (
      <div className="Joke">
        <div className="JokeIcon">
          <img src={this.props.joke.icon_url} alt=""/>
        </div>
        <div className="JokeValue">
          {this.props.joke.value}
        </div>
      </div>
    );
  }
}

export default Joke;