import React, { Component, Fragment } from 'react';
import './WatchList.css';

class WatchList extends Component {
  shouldComponentUpdate(newProps) {
    return (newProps.movie.name !== this.props.movie.name);
  }

  render() {
    return (
      <Fragment>
        <input
          type="text"
          className="ItemInput"
          value={this.props.movie.name}
          onChange={(event) => this.props.change(event, this.props.movie.id)}
        />
        <div className="ItemControl">
          <img
            src="img/ic-del.png"
            className="IconRemove"
            alt="Remove"
            title="Remove this movie"
            onClick={() => this.props.remove(this.props.movie.id)}
          />
        </div>
      </Fragment>
    );
  }
}

export default WatchList;