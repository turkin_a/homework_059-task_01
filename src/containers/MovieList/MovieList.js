import React, { Component } from 'react';
import './MovieList.css';
import InputMovie from "../../components/InputMovie/InputMovie";
import WatchList from "../../components/WatchList/WatchList";

class MovieList extends Component {
  state = {
    movieList: [],
    newMovie: ''
  };

  handleNewMovie = (event) => {
    const newMovie = event.target.value;

    this.setState({newMovie});
  };

  handleChangeMovie = (event, id) => {
    const index = this.state.movieList.findIndex(m => m.id === id);
    const movieList = [...this.state.movieList];
    const changingMovie = {...movieList[index]};
    changingMovie.name = event.target.value;
    movieList[index] = changingMovie;
    localStorage.setItem('movieList', JSON.stringify(movieList));

    this.setState({movieList});
  };

  addMovie = () => {
    if (this.state.newMovie === '') return null;

    const movieList = [...this.state.movieList];
    movieList.push({name: this.state.newMovie, id: Date.now()});
    localStorage.setItem('movieList', JSON.stringify(movieList));

    this.setState({movieList, newMovie: ''});
  };

  removeMovie = (id) => {
    const index = this.state.movieList.findIndex(m => m.id === id);
    const movieList = [...this.state.movieList];
    movieList.splice(index, 1);
    localStorage.setItem('movieList', JSON.stringify(movieList));

    this.setState({movieList});
  };

  componentDidMount() {
    const movieList = JSON.parse(localStorage.getItem('movieList'));

    if (movieList) this.setState({movieList});
  }

  render() {
    return (
      <div className="MovieList">
        <InputMovie
          value={this.state.newMovie}
          change={this.handleNewMovie}
          click={this.addMovie}
        />
        <div className="WatchList">
          {this.state.movieList.map(movie => {
            return (
              <WatchList
                key={movie.id}
                movie={movie}
                change={this.handleChangeMovie}
                remove={this.removeMovie}
              />
            );
          })}
        </div>
      </div>
    );
  }
}

export default MovieList;