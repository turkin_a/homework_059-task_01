import React, { Component } from 'react';
import './JokeList.css';
import Joke from "../../components/Joke/Joke";
import Button from "../../components/UI/Button/Button";

class JokeList extends Component {
  state = {
    jokes: []
  };

  getJokes = () => {
    const JOKES_URL = 'https://api.chucknorris.io/jokes/random';
    let jokes = [];

    for (let i = 0; i < 5; i++) {
      jokes.push(fetch(JOKES_URL).then(response => {
        if (response.ok) {
          return response.json();
        }
        throw new Error('Something went wrong with network request');
      }).catch(error => {
        console.log(error);
      }));
    }

    Promise.all(jokes).then(jokes => {
      this.setState(prevState => {
        return prevState.jokes = jokes;
      });
    });
  };

  componentDidMount() {
    this.getJokes();
  }

  render() {
    return (
      <div className="JokeList">
        <Button click={this.getJokes}>Refresh</Button>
        {this.state.jokes.map(joke => {
          return <Joke key={joke.id} joke={joke}/>;
        })}
      </div>
    );
  }
}

export default JokeList;