import React, { Component } from 'react';
import MovieList from "./containers/MovieList/MovieList";
import JokeList from "./containers/JokeList/JokeList";

class App extends Component {
  render() {
    return <MovieList />; // Task 01
    // return <JokeList />;  // Task 02
  }
}

export default App;
